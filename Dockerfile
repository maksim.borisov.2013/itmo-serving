FROM python:3.10

#
WORKDIR /code

#
RUN pip install --upgrade pip

# #
COPY ./requirements.txt /code/

COPY ./inference.py /code/app/inference.py
#COPY ./.env /code/app/.env

RUN pip install -r /code/requirements.txt


CMD ["uvicorn", "app.inference:app", "--host", "0.0.0.0", "--port", "80"]
